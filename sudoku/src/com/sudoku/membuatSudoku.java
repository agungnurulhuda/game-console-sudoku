package com.sudoku;
import java.util.Scanner;

public class membuatSudoku {
    Scanner scan = new Scanner(System.in);
    boolean isCompleted = true;
    boolean loser = true;
    int nilai = 0;
    String pilih = "";
//    public int[] board = {0,3,6,5,7,4,2,8,1,9,
//                            8,4,2,3,1,9,7,5,6,
//                            9,7,1,8,5,6,3,4,2,
//                            4,5,6,9,7,8,1,2,3,  untuk mempermudah pengetesan apakah program
//                            1,3,9,4,2,5,6,7,8,  berjalan dengan baik sampai berakhir
//                            2,8,7,6,3,1,5,9,4,  bapak bisa mengggunakan array ini dan tinggal
//                            6,1,4,5,9,3,2,8,7,  menginputkan angka 5 pada posisi ii
//                            5,9,3,2,8,7,4,6,1,
//                            7,2,8,1,6,4,9,3,0};
    public int[] board = new int[]{0,0,0,0,0,0,0,0,0,0,
                                     0,0,0,0,0,0,0,0,0,
                                     0,0,0,0,0,0,0,0,0,
                                     0,0,0,0,0,0,0,0,0,
                                     0,0,0,0,0,0,0,0,0,
                                     0,0,0,0,0,0,0,0,0,
                                     0,0,0,0,0,0,0,0,0,
                                     0,0,0,0,0,0,0,0,0,
                                     0,0,0,0,0,0,0,0,0};

    int vertikal1 = 0;
    int vertikal2 = 0;
    int vertikal3 = 0;
    int vertikal4 = 0;
    int vertikal5 = 0;
    int vertikal6 = 0;
    int vertikal7 = 0;
    int vertikal8 = 0;
    int vertikal9 = 0;

    int horizontal1 = 0;
    int horizontal2 = 0;
    int horizontal3 = 0;
    int horizontal4 = 0;
    int horizontal5 = 0;
    int horizontal6 = 0;
    int horizontal7 = 0;
    int horizontal8 = 0;
    int horizontal9 = 0;

    int kotak1 = 0;
    int kotak2 = 0;
    int kotak3 = 0;
    int kotak4 = 0;
    int kotak5 = 0;
    int kotak6 = 0;
    int kotak7 = 0;
    int kotak8 = 0;
    int kotak9 = 0;

    public void display(){
        System.out.println("    A   B   C    D   E   F    G   H   I");
        //line1 (1-9)
        System.out.println("  ._____________________________________.");
        System.out.print("A | " + board[1] +" | " + board[2] + " | " + board[3] +" || "+ board[4] +" | "+ board[5]);
        System.out.println(" | " + board[6] +" || " + board[7] + " | " + board[8] +" | "+ board[9] +" | ");
        //line2 (10-18)
        System.out.println("  ._____________________________________.");
        System.out.print("B | " + board[10] +" | " + board[11] + " | " + board[12] +" || "+ board[13] +" | "+ board[14]);
        System.out.println(" | " + board[15] +" || " + board[16] + " | " + board[17] +" | "+ board[18] +" | ");
        //line3 (19-27)
        System.out.println("  ._____________________________________.");
        System.out.print("C | " + board[19] +" | " + board[20] + " | " + board[21] +" || "+ board[22] +" | "+ board[23]);
        System.out.println(" | " + board[24] +" || " + board[25] + " | " + board[26] +" | "+ board[27] +" | ");
        //line4 (28-36)
        System.out.println("  ._____________________________________.");
        System.out.println("  ._____________________________________.");
        System.out.print("D | " + board[28] +" | " + board[29] + " | " + board[30] +" || "+ board[31] +" | "+ board[32]);
        System.out.println(" | " + board[33] +" || " + board[34] + " | " + board[35] +" | "+ board[36] +" | ");
        //line5 (37-45)
        System.out.println("  ._____________________________________.");
        System.out.print("E | " + board[37] +" | " + board[38] + " | " + board[39] +" || "+ board[40] +" | "+ board[41]);
        System.out.println(" | " + board[42] +" || " + board[43] + " | " + board[44] +" | "+ board[45] +" | ");
        //line6 (46-54)
        System.out.println("  ._____________________________________.");
        System.out.print("F | " + board[46] +" | " + board[47] + " | " + board[48] +" || "+ board[49] +" | "+ board[50]);
        System.out.println(" | " + board[51] +" || " + board[52] + " | " + board[53] +" | "+ board[54] +" | ");
        //line7 (55-63)
        System.out.println("  ._____________________________________.");
        System.out.println("  ._____________________________________.");
        System.out.print("G | " + board[55] +" | " + board[56] + " | " + board[57] +" || "+ board[58] +" | "+ board[59]);
        System.out.println(" | " + board[60] +" || " + board[61] + " | " + board[62] +" | "+ board[63] +" | ");
        //line8 (64-72)
        System.out.println("  ._____________________________________.");
        System.out.print("H | " + board[64] +" | " + board[65] + " | " + board[66] +" || "+ board[67] +" | "+ board[68]);
        System.out.println(" | " + board[69] +" || " + board[70] + " | " + board[71] +" | "+ board[72] +" | ");
        //line9 (73-81)
        System.out.println("  ._____________________________________.");
        System.out.print("I | " + board[73] +" | " + board[74] + " | " + board[75] +" || "+ board[76] +" | "+ board[77]);
        System.out.println(" | " + board[78] +" || " + board[79] + " | " + board[80] +" | "+ board[81] +" | ");
        System.out.println("  ._____________________________________.");
    }

    public void analisis(){
        //HORIZONTAL
        horizontal1 = board[1]+board[2]+board[3]+board[4]+board[5]+board[6]+board[7]+board[8]+board[9];
        horizontal2 = board[10]+board[11]+board[12]+board[13]+board[14]+board[15]+board[16]+board[17]+board[18];
        horizontal3 = board[19]+board[20]+board[21]+board[22]+board[23]+board[24]+board[25]+board[26]+board[27];
        horizontal4 = board[28]+board[29]+board[30]+board[31]+board[32]+board[33]+board[34]+board[35]+board[36];
        horizontal5 = board[37]+board[38]+board[39]+board[40]+board[41]+board[42]+board[43]+board[44]+board[45];
        horizontal6 = board[46]+board[47]+board[48]+board[49]+board[50]+board[51]+board[52]+board[53]+board[54];
        horizontal7 = board[55]+board[56]+board[57]+board[58]+board[59]+board[60]+board[61]+board[62]+board[63];
        horizontal8 = board[64]+board[65]+board[66]+board[67]+board[68]+board[69]+board[70]+board[71]+board[72];
        horizontal9 = board[73]+board[74]+board[75]+board[76]+board[77]+board[78]+board[79]+board[80]+board[81];
        //VERTICAL
        vertikal1 = board[1]+board[10]+board[19]+board[28]+board[37]+board[46]+board[55]+board[64]+board[73];
        vertikal2 = board[2]+board[11]+board[20]+board[29]+board[38]+board[47]+board[56]+board[65]+board[74];
        vertikal3 = board[3]+board[12]+board[21]+board[30]+board[39]+board[48]+board[57]+board[66]+board[75];
        vertikal4 = board[4]+board[13]+board[22]+board[31]+board[40]+board[49]+board[58]+board[67]+board[76];
        vertikal5 = board[5]+board[14]+board[23]+board[32]+board[41]+board[50]+board[59]+board[68]+board[77];
        vertikal6 = board[6]+board[15]+board[24]+board[33]+board[42]+board[51]+board[60]+board[69]+board[78];
        vertikal7 = board[7]+board[16]+board[25]+board[34]+board[43]+board[52]+board[61]+board[70]+board[79];
        vertikal8 = board[8]+board[17]+board[26]+board[35]+board[44]+board[53]+board[62]+board[71]+board[80];
        vertikal9 = board[9]+board[18]+board[27]+board[36]+board[45]+board[54]+board[63]+board[72]+board[81];
        //KOTAK 3X3
        kotak1 = board[1]+board[2]+board[3]+board[10]+board[11]+board[12]+board[19]+board[20]+board[21];
        kotak2 = board[4]+board[5]+board[6]+board[13]+board[14]+board[15]+board[22]+board[23]+board[24];
        kotak3 = board[7]+board[8]+board[9]+board[16]+board[17]+board[18]+board[25]+board[26]+board[27];
        kotak4 = board[28]+board[29]+board[30]+board[37]+board[38]+board[39]+board[46]+board[47]+board[48];
        kotak5 = board[31]+board[32]+board[33]+board[40]+board[41]+board[42]+board[49]+board[50]+board[51];
        kotak6 = board[34]+board[35]+board[36]+board[43]+board[44]+board[45]+board[52]+board[53]+board[54];
        kotak7 = board[55]+board[56]+board[57]+board[64]+board[65]+board[66]+board[73]+board[74]+board[75];
        kotak8 = board[58]+board[59]+board[60]+board[67]+board[68]+board[69]+board[76]+board[77]+board[78];
        kotak9 = board[61]+board[62]+board[63]+board[70]+board[71]+board[72]+board[79]+board[80]+board[81];

        if(horizontal1 == 45 && horizontal2 == 45 && horizontal3 == 45 && horizontal4 == 45 && horizontal5 == 45 && horizontal6 == 45 && horizontal7 == 45 && horizontal8 == 45 && horizontal9 == 45 ){
            if(vertikal1 == 45 && vertikal2 == 45 && vertikal3 == 45 && vertikal4 == 45 && vertikal5 == 45 && vertikal6 == 45 && vertikal7 == 45 && vertikal8 == 45 && vertikal9 == 45 ){
                if(kotak1 == 45 && kotak2 == 45 && kotak3 == 45 && kotak4 == 45 && kotak5 == 45 && kotak6 == 45 && kotak7 == 45 && kotak8 == 45 && kotak9 == 45 ){
                    System.out.println("\n");
                    System.out.println("\t\t||================================================||");
                    System.out.println("\t\t||           < < <  Y O U  W I N  > > >           ||");
                    System.out.println("\t\t||________________________________________________||");
                    System.out.println("\t\t||                                                ||");
                    System.out.println("\t\t||                   S U D O K U                  ||");
                    System.out.println("\t\t||                C O M P L E T E D               ||");
                    System.out.println("\t\t||                                                ||");
                    System.out.println("\t\t||================================================||");
                    isCompleted=false;
                    System.exit(1);
                }
            }
        }

    }

    public void pilihLetak(){
        System.out.print("masukan posisi : ");
        pilih = scan.next();
        if(pilih.length() != 2){
            System.out.println();
            System.out.println(".-----------------------------------------------------.");
            System.out.println("| Inputan yang anda masukan harus sebanyak 2 karakter |");
            System.out.println("|    dengan isi karakter baris A-I & kolom dari A-I   |");
            System.out.println("._____________________________________________________.");
        }else{
            try{
                System.out.print("masukan angka  : ");
                nilai = scan.nextInt();
            }catch (Exception e){
                System.out.println("masukan data berypa integer");
            }

            if(nilai > 9 || nilai < 1){
                System.out.println(".--------------------------.");
                System.out.println("| input angka antara 1 - 9 |");
                System.out.println(".__________________________.");
            }else{
                switch (pilih) {
                    case "AA", "Aa", "aa", "aA" -> board[1] = nilai;
                    case "AB", "Ab", "ab", "aB" -> board[2] = nilai;
                    case "AC", "Ac", "ac", "aC" -> board[3] = nilai;
                    case "AD", "Ad", "ad", "aD" -> board[4] = nilai;
                    case "AE", "Ae", "ae", "aE" -> board[5] = nilai;
                    case "AF", "Af", "af", "aF" -> board[6] = nilai;
                    case "AG", "Ag", "ag", "aG" -> board[7] = nilai;
                    case "AH", "Ah", "ah", "aH" -> board[8] = nilai;
                    case "AI", "Ai", "ai", "aI" -> board[9] = nilai;
                    case "BA", "Ba", "ba", "bA" -> board[10] = nilai;
                    case "BB", "Bb", "bb", "bB" -> board[11] = nilai;
                    case "BC", "Bc", "bc", "bC" -> board[12] = nilai;
                    case "BD", "Bd", "bd", "bD" -> board[13] = nilai;
                    case "BE", "Be", "be", "bE" -> board[14] = nilai;
                    case "BF", "Bf", "bf", "bF" -> board[15] = nilai;
                    case "BG", "Bg", "bg", "bG" -> board[16] = nilai;
                    case "BH", "Bh", "bh", "bH" -> board[17] = nilai;
                    case "BI", "Bi", "bi", "bI" -> board[18] = nilai;
                    case "CA", "Ca", "ca", "cA" -> board[19] = nilai;
                    case "CB", "Cb", "cb", "cB" -> board[20] = nilai;
                    case "CC", "Cc", "cc", "cC" -> board[21] = nilai;
                    case "CD", "Cd", "cd", "cD" -> board[22] = nilai;
                    case "CE", "Ce", "ce", "cE" -> board[23] = nilai;
                    case "CF", "Cf", "cf", "cF" -> board[24] = nilai;
                    case "CG", "Cg", "cg", "cG" -> board[25] = nilai;
                    case "CH", "Ch", "ch", "cH" -> board[26] = nilai;
                    case "CI", "Ci", "ci", "cI" -> board[27] = nilai;
                    case "DA", "Da", "da", "dA" -> board[28] = nilai;
                    case "DB", "Db", "db", "dB" -> board[29] = nilai;
                    case "DC", "Dc", "dc", "dC" -> board[30] = nilai;
                    case "DD", "Dd", "dd", "dD" -> board[31] = nilai;
                    case "DE", "De", "de", "dE" -> board[32] = nilai;
                    case "DF", "Df", "df", "dF" -> board[33] = nilai;
                    case "DG", "Dg", "dg", "dG" -> board[34] = nilai;
                    case "DH", "Dh", "dh", "dH" -> board[35] = nilai;
                    case "DI", "Di", "di", "dI" -> board[36] = nilai;
                    case "EA", "Ea", "ea", "eA" -> board[37] = nilai;
                    case "EB", "Eb", "eb", "eB" -> board[38] = nilai;
                    case "EC", "Ec", "ec", "eC" -> board[39] = nilai;
                    case "ED", "Ed", "ed", "eD" -> board[40] = nilai;
                    case "EE", "Ee", "ee", "eE" -> board[41] = nilai;
                    case "EF", "Ef", "ef", "eF" -> board[42] = nilai;
                    case "EG", "Eg", "eg", "eG" -> board[43] = nilai;
                    case "EH", "Eh", "eh", "eH" -> board[44] = nilai;
                    case "EI", "Ei", "ei", "eI" -> board[45] = nilai;
                    case "FA", "Fa", "fa", "fA" -> board[46] = nilai;
                    case "FB", "Fb", "fb", "fB" -> board[47] = nilai;
                    case "FC", "Fc", "fc", "fC" -> board[48] = nilai;
                    case "FD", "Fd", "fd", "fD" -> board[49] = nilai;
                    case "FE", "Fe", "fe", "fE" -> board[50] = nilai;
                    case "FF", "Ff", "ff", "fF" -> board[51] = nilai;
                    case "FG", "Fg", "fg", "fG" -> board[52] = nilai;
                    case "FH", "Fh", "fh", "fH" -> board[53] = nilai;
                    case "FI", "Fi", "fi", "fI" -> board[54] = nilai;
                    case "GA", "Ga", "ga", "gA" -> board[55] = nilai;
                    case "GB", "Gb", "gb", "gB" -> board[56] = nilai;
                    case "GC", "Gc", "gc", "gC" -> board[57] = nilai;
                    case "GD", "Gd", "gd", "gD" -> board[58] = nilai;
                    case "GE", "Ge", "ge", "gE" -> board[59] = nilai;
                    case "GF", "Gf", "gf", "gF" -> board[60] = nilai;
                    case "GG", "Gg", "gg", "gG" -> board[61] = nilai;
                    case "GH", "Gh", "gh", "gH" -> board[62] = nilai;
                    case "GI", "Gi", "gi", "gI" -> board[63] = nilai;
                    case "HA", "Ha", "ha", "hA" -> board[64] = nilai;
                    case "HB", "Hb", "hb", "hB" -> board[65] = nilai;
                    case "HC", "Hc", "hc", "hC" -> board[66] = nilai;
                    case "HD", "Hd", "hd", "hD" -> board[67] = nilai;
                    case "HE", "He", "he", "hE" -> board[68] = nilai;
                    case "HF", "Hf", "hf", "hF" -> board[69] = nilai;
                    case "HG", "Hg", "hg", "hG" -> board[70] = nilai;
                    case "HH", "Hh", "hh", "hH" -> board[71] = nilai;
                    case "HI", "Hi", "hi", "hI" -> board[72] = nilai;
                    case "IA", "Ia", "ia", "iA" -> board[73] = nilai;
                    case "IB", "Ib", "ib", "iB" -> board[74] = nilai;
                    case "IC", "Ic", "ic", "iC" -> board[75] = nilai;
                    case "ID", "Id", "id", "iD" -> board[76] = nilai;
                    case "IE", "Ie", "ie", "iE" -> board[77] = nilai;
                    case "IF", "If", "if", "iF" -> board[78] = nilai;
                    case "IG", "Ig", "ig", "iG" -> board[79] = nilai;
                    case "IH", "Ih", "ih", "iH" -> board[80] = nilai;
                    case "II", "Ii", "ii", "iI" -> board[81] = nilai;
                    default -> System.out.println("tidak terdaftar");
                }
            }
        }
    }

    public void gameOver(){
        System.out.println(".===========================================.");
        System.out.println("||      _____________ . _____________      ||");
        System.out.println("||     |  - - G A M E   O V E R - -  |     ||");
        System.out.println("||      -----------------------------      ||");
        System.out.println(".===========================================.");
        loser = false;
    }

    public void analisis2(){
        if(board[1] == board[2] && board[1] != 0 || board[1] == board[3] && board[1] != 0 || board[1] == board[4] && board[1] != 0 || board[1] == board[5] && board[1] != 0 || board[1] == board[6] && board[1] != 0 || board[1] == board[7] && board[1] != 0 || board[1] == board[8] && board[1] != 0 || board[1] == board[9] && board[1] != 0){
            gameOver();
        }else if(board[2] == board[1] && board[2] != 0 || board[2] == board[3] && board[2] != 0 || board[2] == board[4] && board[2] != 0 || board[2] == board[5] && board[2] != 0 || board[2] == board[6] && board[2] != 0 || board[2] == board[7] && board[2] != 0 || board[2] == board[8] && board[2] != 0 || board[2] == board[9] && board[2] != 0){
            gameOver();
        }else if(board[3] == board[1] && board[3] != 0 || board[3] == board[2] && board[3] != 0 || board[3] == board[4] && board[3] != 0 || board[3] == board[5] && board[3] != 0 || board[3] == board[6] && board[3] != 0 || board[3] == board[7] && board[3] != 0 || board[3] == board[8] && board[3] != 0 || board[3] == board[9] && board[3] != 0){
            gameOver();
        }else if(board[4] == board[1] && board[4] != 0 || board[4] == board[2] && board[4] != 0 || board[4] == board[3] && board[4] != 0 || board[4] == board[5] && board[4] != 0 || board[4] == board[6] && board[4] != 0 || board[4] == board[7] && board[4] != 0 || board[4] == board[8] && board[4] != 0 || board[4] == board[9] && board[4] != 0){
            gameOver();
        }else if(board[5] == board[1] && board[5] != 0 || board[5] == board[2] && board[5] != 0 || board[5] == board[3] && board[5] != 0 || board[5] == board[4] && board[5] != 0 || board[5] == board[6] && board[5] != 0 || board[5] == board[7] && board[5] != 0 || board[5] == board[8] && board[5] != 0 || board[5] == board[9] && board[5] != 0){
            gameOver();
        }else if(board[6] == board[1] && board[6] != 0 || board[6] == board[2] && board[6] != 0 || board[6] == board[3] && board[6] != 0 || board[6] == board[4] && board[6] != 0 || board[6] == board[5] && board[6] != 0 || board[6] == board[7] && board[6] != 0 || board[6] == board[8] && board[6] != 0 || board[6] == board[9] && board[6] != 0){
            gameOver();
        }else if(board[7] == board[1] && board[7] != 0 || board[7] == board[2] && board[7] != 0 || board[7] == board[3] && board[7] != 0 || board[7] == board[4] && board[7] != 0 || board[7] == board[5] && board[7] != 0 || board[7] == board[6] && board[7] != 0 || board[7] == board[8] && board[7] != 0 || board[7] == board[9] && board[7] != 0){
            gameOver();
        }else if(board[8] == board[1] && board[8] != 0 || board[8] == board[2] && board[8] != 0 || board[8] == board[3] && board[8] != 0 || board[8] == board[4] && board[8] != 0 || board[8] == board[5] && board[8] != 0 || board[8] == board[6] && board[8] != 0 || board[8] == board[7] && board[8] != 0 || board[8] == board[9] && board[8] != 0){
            gameOver();
        }else if(board[9] == board[1] && board[9] != 0 || board[9] == board[2] && board[9] != 0 || board[9] == board[3] && board[9] != 0 || board[9] == board[4] && board[9] != 0 || board[9] == board[5] && board[9] != 0 || board[9] == board[6] && board[9] != 0 || board[9] == board[7] && board[9] != 0 || board[9] == board[8] && board[9] != 0){
            gameOver();
        }

        else if(board[10] == board[11] && board[10] != 0 || board[10] == board[12] && board[10] != 0 || board[10] == board[13] && board[10] != 0 || board[10] == board[14] && board[10] != 0 || board[10] == board[15] && board[10] != 0 || board[10] == board[16] && board[10] != 0 || board[10] == board[17] && board[10] != 0 || board[10] == board[18] && board[10] != 0){
            gameOver();
        }else if(board[11] == board[10] && board[11] != 0 || board[11] == board[12] && board[11] != 0 || board[11] == board[13] && board[11] != 0 || board[11] == board[14] && board[11] != 0 || board[11] == board[15] && board[11] != 0 || board[11] == board[16] && board[11] != 0 || board[11] == board[17] && board[11] != 0 || board[11] == board[18] && board[11] != 0){
            gameOver();
        }else if(board[12] == board[10] && board[12] != 0 || board[12] == board[11] && board[12] != 0 || board[12] == board[13] && board[12] != 0 || board[12] == board[14] && board[12] != 0 || board[12] == board[15] && board[12] != 0 || board[12] == board[16] && board[12] != 0 || board[12] == board[17] && board[12] != 0 || board[12] == board[18] && board[12] != 0){
            gameOver();
        }else if(board[13] == board[10] && board[13] != 0 || board[13] == board[11] && board[13] != 0 || board[13] == board[12] && board[13] != 0 || board[13] == board[14] && board[13] != 0 || board[13] == board[15] && board[13] != 0 || board[13] == board[16] && board[13] != 0 || board[13] == board[17] && board[13] != 0 || board[13] == board[18] && board[13] != 0){
            gameOver();
        }else if(board[14] == board[10] && board[14] != 0 || board[14] == board[11] && board[14] != 0 || board[14] == board[12] && board[14] != 0 || board[14] == board[13] && board[14] != 0 || board[14] == board[15] && board[14] != 0 || board[14] == board[16] && board[14] != 0 || board[14] == board[17] && board[14] != 0 || board[14] == board[18] && board[14] != 0){
            gameOver();
        }else if(board[15] == board[10] && board[15] != 0 || board[15] == board[11] && board[15] != 0 || board[15] == board[12] && board[15] != 0 || board[15] == board[13] && board[15] != 0 || board[15] == board[14] && board[15] != 0 || board[15] == board[16] && board[15] != 0 || board[15] == board[17] && board[15] != 0 || board[15] == board[18] && board[15] != 0){
            gameOver();
        }else if(board[16] == board[10] && board[16] != 0 || board[16] == board[11] && board[16] != 0 || board[16] == board[12] && board[16] != 0 || board[16] == board[13] && board[16] != 0 || board[16] == board[14] && board[16] != 0 || board[16] == board[15] && board[16] != 0 || board[16] == board[17] && board[16] != 0 || board[16] == board[18] && board[16] != 0){
            gameOver();
        }else if(board[17] == board[10] && board[17] != 0 || board[17] == board[11] && board[17] != 0 || board[17] == board[12] && board[17] != 0 || board[17] == board[13] && board[17] != 0 || board[17] == board[14] && board[17] != 0 || board[17] == board[15] && board[17] != 0 || board[17] == board[16] && board[17] != 0 || board[17] == board[18] && board[17] != 0){
            gameOver();
        }else if(board[18] == board[10] && board[18] != 0 || board[18] == board[11] && board[18] != 0 || board[18] == board[12] && board[18] != 0 || board[18] == board[13] && board[18] != 0 || board[18] == board[14] && board[18] != 0 || board[18] == board[15] && board[18] != 0 || board[18] == board[16] && board[18] != 0 || board[18] == board[17] && board[18] != 0){
            gameOver();
        }

        else if(board[19] == board[20] && board[19] != 0 || board[19] == board[21] && board[19] != 0 || board[19] == board[22] && board[19] != 0 || board[19] == board[23] && board[19] != 0 || board[19] == board[24] && board[19] != 0 || board[19] == board[25] && board[19] != 0 || board[19] == board[26] && board[19] != 0 || board[19] == board[27] && board[19] != 0){
            gameOver();
        }else if(board[20] == board[19] && board[20] != 0 || board[20] == board[21] && board[20] != 0 || board[20] == board[22] && board[20] != 0 || board[20] == board[23] && board[20] != 0 || board[20] == board[24] && board[20] != 0 || board[20] == board[25] && board[20] != 0 || board[20] == board[26] && board[20] != 0 || board[20] == board[27] && board[20] != 0){
            gameOver();
        }else if(board[21] == board[19] && board[21] != 0 || board[21] == board[20] && board[21] != 0 || board[21] == board[22] && board[21] != 0 || board[21] == board[23] && board[21] != 0 || board[21] == board[24] && board[21] != 0 || board[21] == board[25] && board[21] != 0 || board[21] == board[26] && board[21] != 0 || board[21] == board[27] && board[21] != 0){
            gameOver();
        }else if(board[22] == board[19] && board[22] != 0 || board[22] == board[20] && board[22] != 0 || board[22] == board[21] && board[22] != 0 || board[22] == board[23] && board[22] != 0 || board[22] == board[24] && board[22] != 0 || board[22] == board[25] && board[22] != 0 || board[22] == board[26] && board[22] != 0 || board[22] == board[27] && board[22] != 0){
            gameOver();
        }else if(board[23] == board[19] && board[23] != 0 || board[23] == board[20] && board[23] != 0 || board[23] == board[21] && board[23] != 0 || board[23] == board[22] && board[23] != 0 || board[23] == board[24] && board[23] != 0 || board[23] == board[25] && board[23] != 0 || board[23] == board[26] && board[23] != 0 || board[23] == board[27] && board[23] != 0){
            gameOver();
        }else if(board[24] == board[19] && board[24] != 0 || board[24] == board[20] && board[24] != 0 || board[24] == board[21] && board[24] != 0 || board[24] == board[22] && board[24] != 0 || board[24] == board[23] && board[24] != 0 || board[24] == board[25] && board[24] != 0 || board[24] == board[26] && board[24] != 0 || board[24] == board[27] && board[24] != 0){
            gameOver();
        }else if(board[25] == board[19] && board[25] != 0 || board[25] == board[20] && board[25] != 0 || board[25] == board[21] && board[25] != 0 || board[25] == board[22] && board[25] != 0 || board[25] == board[23] && board[25] != 0 || board[25] == board[24] && board[25] != 0 || board[25] == board[26] && board[25] != 0 || board[25] == board[27] && board[25] != 0){
            gameOver();
        }else if(board[26] == board[19] && board[26] != 0 || board[26] == board[20] && board[26] != 0 || board[26] == board[21] && board[26] != 0 || board[26] == board[22] && board[26] != 0 || board[26] == board[23] && board[26] != 0 || board[26] == board[24] && board[26] != 0 || board[26] == board[25] && board[26] != 0 || board[26] == board[27] && board[26] != 0){
            gameOver();
        }else if(board[27] == board[19] && board[27] != 0 || board[27] == board[20] && board[27] != 0 || board[27] == board[21] && board[27] != 0 || board[27] == board[22] && board[27] != 0 || board[27] == board[23] && board[27] != 0 || board[27] == board[24] && board[27] != 0 || board[27] == board[25] && board[27] != 0 || board[27] == board[26] && board[27] != 0){
            gameOver();
        }
    }

    public void analisis3(){
        if(board[28] == board[29] && board[28] != 0 || board[28] == board[30] && board[28] != 0 || board[28] == board[31] && board[28] != 0 || board[28] == board[32] && board[28] != 0 || board[28] == board[33] && board[28] != 0 || board[28] == board[34] && board[28] != 0 || board[28] == board[35] && board[28] != 0 || board[28] == board[36] && board[28] != 0){
            gameOver();
        }else if(board[29] == board[28] && board[29] != 0 || board[29] == board[30] && board[29] != 0 || board[29] == board[31] && board[29] != 0 || board[29] == board[32] && board[29] != 0 || board[29] == board[33] && board[29] != 0 || board[29] == board[34] && board[29] != 0 || board[29] == board[35] && board[29] != 0 || board[29] == board[36] && board[29] != 0){
            gameOver();
        }else if(board[30] == board[28] && board[30] != 0 || board[30] == board[29] && board[30] != 0 || board[30] == board[31] && board[30] != 0 || board[30] == board[32] && board[30] != 0 || board[30] == board[33] && board[30] != 0 || board[30] == board[34] && board[30] != 0 || board[30] == board[35] && board[30] != 0 || board[30] == board[36] && board[30] != 0){
            gameOver();
        }else if(board[31] == board[28] && board[31] != 0 || board[31] == board[29] && board[31] != 0 || board[31] == board[30] && board[31] != 0 || board[31] == board[32] && board[31] != 0 || board[31] == board[33] && board[31] != 0 || board[31] == board[34] && board[31] != 0 || board[31] == board[35] && board[31] != 0 || board[31] == board[36] && board[31] != 0){
            gameOver();
        }else if(board[32] == board[28] && board[32] != 0 || board[32] == board[29] && board[32] != 0 || board[32] == board[30] && board[32] != 0 || board[32] == board[31] && board[32] != 0 || board[32] == board[33] && board[32] != 0 || board[32] == board[34] && board[32] != 0 || board[32] == board[35] && board[32] != 0 || board[32] == board[36] && board[32] != 0){
            gameOver();
        }else if(board[33] == board[28] && board[33] != 0 || board[33] == board[29] && board[33] != 0 || board[33] == board[30] && board[33] != 0 || board[33] == board[31] && board[33] != 0 || board[33] == board[32] && board[33] != 0 || board[33] == board[34] && board[33] != 0 || board[33] == board[35] && board[33] != 0 || board[33] == board[36] && board[33] != 0){
            gameOver();
        }else if(board[34] == board[28] && board[34] != 0 || board[34] == board[29] && board[34] != 0 || board[34] == board[30] && board[34] != 0 || board[34] == board[31] && board[34] != 0 || board[34] == board[32] && board[34] != 0 || board[34] == board[33] && board[34] != 0 || board[34] == board[35] && board[34] != 0 || board[34] == board[36] && board[34] != 0){
            gameOver();
        }else if(board[35] == board[28] && board[35] != 0 || board[35] == board[29] && board[35] != 0 || board[35] == board[30] && board[35] != 0 || board[35] == board[31] && board[35] != 0 || board[35] == board[32] && board[35] != 0 || board[35] == board[33] && board[35] != 0 || board[35] == board[34] && board[35] != 0 || board[35] == board[36] && board[35] != 0){
            gameOver();
        }else if(board[36] == board[28] && board[36] != 0 || board[36] == board[29] && board[36] != 0 || board[36] == board[30] && board[36] != 0 || board[36] == board[31] && board[36] != 0 || board[35] == board[32] && board[36] != 0 || board[36] == board[33] && board[36] != 0 || board[36] == board[34] && board[36] != 0 || board[36] == board[35] && board[36] != 0){
            gameOver();
        }

        else if(board[37] == board[38] && board[37] != 0 || board[37] == board[39] && board[37] != 0 || board[37] == board[40] && board[37] != 0 || board[37] == board[41] && board[37] != 0 || board[37] == board[42] && board[37] != 0 || board[37] == board[43] && board[37] != 0 || board[37] == board[44] && board[37] != 0 || board[37] == board[45] && board[37] != 0){
            gameOver();
        }else if(board[38] == board[37] && board[38] != 0 || board[38] == board[39] && board[38] != 0 || board[38] == board[40] && board[38] != 0 || board[38] == board[41] && board[38] != 0 || board[38] == board[42] && board[38] != 0 || board[38] == board[43] && board[38] != 0 || board[38] == board[44] && board[38] != 0 || board[38] == board[45] && board[38] != 0){
            gameOver();
        }else if(board[39] == board[37] && board[39] != 0 || board[39] == board[38] && board[39] != 0 || board[39] == board[40] && board[39] != 0 || board[39] == board[41] && board[39] != 0 || board[39] == board[42] && board[39] != 0 || board[39] == board[43] && board[39] != 0 || board[39] == board[44] && board[39] != 0 || board[39] == board[45] && board[39] != 0){
            gameOver();
        }else if(board[40] == board[37] && board[40] != 0 || board[40] == board[38] && board[40] != 0 || board[40] == board[39] && board[40] != 0 || board[40] == board[41] && board[40] != 0 || board[40] == board[42] && board[40] != 0 || board[40] == board[43] && board[40] != 0 || board[40] == board[44] && board[40] != 0 || board[40] == board[45] && board[40] != 0){
            gameOver();
        }else if(board[41] == board[37] && board[41] != 0 || board[41] == board[38] && board[41] != 0 || board[41] == board[39] && board[41] != 0 || board[41] == board[40] && board[41] != 0 || board[41] == board[42] && board[41] != 0 || board[41] == board[43] && board[41] != 0 || board[41] == board[44] && board[41] != 0 || board[41] == board[45] && board[41] != 0){
            gameOver();
        }else if(board[42] == board[37] && board[42] != 0 || board[42] == board[38] && board[42] != 0 || board[42] == board[39] && board[42] != 0 || board[42] == board[40] && board[42] != 0 || board[42] == board[41] && board[42] != 0 || board[42] == board[43] && board[42] != 0 || board[42] == board[44] && board[42] != 0 || board[42] == board[45] && board[42] != 0){
            gameOver();
        }else if(board[43] == board[37] && board[43] != 0 || board[43] == board[38] && board[43] != 0 || board[43] == board[39] && board[43] != 0 || board[43] == board[40] && board[43] != 0 || board[43] == board[41] && board[43] != 0 || board[43] == board[42] && board[43] != 0 || board[43] == board[44] && board[43] != 0 || board[43] == board[45] && board[43] != 0){
            gameOver();
        }else if(board[44] == board[37] && board[44] != 0 || board[44] == board[38] && board[44] != 0 || board[44] == board[39] && board[44] != 0 || board[44] == board[40] && board[44] != 0 || board[44] == board[41] && board[44] != 0 || board[44] == board[42] && board[44] != 0 || board[44] == board[43] && board[44] != 0 || board[44] == board[45] && board[44] != 0){
            gameOver();
        }else if(board[45] == board[37] && board[45] != 0 || board[45] == board[38] && board[45] != 0 || board[45] == board[39] && board[45] != 0 || board[45] == board[40] && board[45] != 0 || board[45] == board[41] && board[45] != 0 || board[45] == board[42] && board[45] != 0 || board[45] == board[43] && board[45] != 0 || board[45] == board[44] && board[45] != 0){
            gameOver();
        }

        else if(board[46] == board[47] && board[46] != 0 || board[46] == board[48] && board[46] != 0 || board[46] == board[49] && board[46] != 0 || board[46] == board[50] && board[46] != 0 || board[46] == board[51] && board[46] != 0 || board[46] == board[52] && board[46] != 0 || board[46] == board[53] && board[46] != 0 || board[46] == board[54] && board[46] != 0){
            gameOver();
        }else if(board[47] == board[46] && board[47] != 0 || board[47] == board[48] && board[47] != 0 || board[47] == board[49] && board[47] != 0 || board[47] == board[50] && board[47] != 0 || board[47] == board[51] && board[47] != 0 || board[47] == board[52] && board[47] != 0 || board[47] == board[53] && board[47] != 0 || board[47] == board[54] && board[47] != 0){
            gameOver();
        }else if(board[48] == board[46] && board[48] != 0 || board[48] == board[47] && board[48] != 0 || board[48] == board[49] && board[48] != 0 || board[48] == board[50] && board[48] != 0 || board[48] == board[51] && board[48] != 0 || board[48] == board[52] && board[48] != 0 || board[48] == board[53] && board[48] != 0 || board[48] == board[54] && board[48] != 0){
            gameOver();
        }else if(board[49] == board[46] && board[49] != 0 || board[49] == board[47] && board[49] != 0 || board[49] == board[48] && board[49] != 0 || board[49] == board[50] && board[49] != 0 || board[49] == board[51] && board[49] != 0 || board[49] == board[52] && board[49] != 0 || board[49] == board[53] && board[49] != 0 || board[49] == board[54] && board[49] != 0){
            gameOver();
        }else if(board[50] == board[46] && board[50] != 0 || board[50] == board[47] && board[50] != 0 || board[50] == board[48] && board[50] != 0 || board[50] == board[49] && board[50] != 0 || board[50] == board[51] && board[50] != 0 || board[50] == board[52] && board[50] != 0 || board[50] == board[53] && board[50] != 0 || board[50] == board[54] && board[50] != 0){
            gameOver();
        }else if(board[51] == board[46] && board[51] != 0 || board[51] == board[47] && board[51] != 0 || board[51] == board[48] && board[51] != 0 || board[51] == board[49] && board[51] != 0 || board[51] == board[50] && board[51] != 0 || board[51] == board[52] && board[51] != 0 || board[51] == board[53] && board[51] != 0 || board[51] == board[54] && board[51] != 0){
            gameOver();
        }else if(board[52] == board[46] && board[52] != 0 || board[52] == board[47] && board[52] != 0 || board[52] == board[48] && board[52] != 0 || board[52] == board[49] && board[52] != 0 || board[52] == board[50] && board[52] != 0 || board[52] == board[51] && board[52] != 0 || board[52] == board[53] && board[52] != 0 || board[52] == board[54] && board[52] != 0){
            gameOver();
        }else if(board[53] == board[46] && board[53] != 0 || board[53] == board[47] && board[53] != 0 || board[53] == board[48] && board[53] != 0 || board[53] == board[49] && board[53] != 0 || board[53] == board[50] && board[53] != 0 || board[53] == board[51] && board[53] != 0 || board[53] == board[52] && board[53] != 0 || board[53] == board[54] && board[53] != 0){
            gameOver();
        }else if(board[54] == board[46] && board[54] != 0 || board[54] == board[47] && board[54] != 0 || board[54] == board[48] && board[54] != 0 || board[54] == board[49] && board[54] != 0 || board[54] == board[50] && board[54] != 0 || board[54] == board[51] && board[54] != 0 || board[54] == board[52] && board[54] != 0 || board[54] == board[53] && board[54] != 0){
            gameOver();
        }
    }

    public void analisis4(){
        if(board[55] == board[56] && board[55] != 0 || board[55] == board[57] && board[55] != 0 || board[55] == board[58] && board[55] != 0 || board[55] == board[59] && board[55] != 0 || board[55] == board[60] && board[55] != 0 || board[55] == board[61] && board[55] != 0 || board[55] == board[62] && board[55] != 0 || board[55] == board[63] && board[55] != 0){
            gameOver();
        }else if(board[56] == board[55] && board[56] != 0 || board[56] == board[57] && board[56] != 0 || board[56] == board[58] && board[56] != 0 || board[56] == board[59] && board[56] != 0 || board[56] == board[60] && board[56] != 0 || board[56] == board[61] && board[56] != 0 || board[56] == board[62] && board[56] != 0 || board[56] == board[63] && board[56] != 0){
            gameOver();
        }else if(board[57] == board[55] && board[57] != 0 || board[57] == board[56] && board[57] != 0 || board[57] == board[58] && board[57] != 0 || board[57] == board[59] && board[57] != 0 || board[57] == board[60] && board[57] != 0 || board[57] == board[61] && board[57] != 0 || board[57] == board[62] && board[57] != 0 || board[57] == board[63] && board[57] != 0){
            gameOver();
        }else if(board[58] == board[55] && board[58] != 0 || board[58] == board[56] && board[58] != 0 || board[58] == board[57] && board[58] != 0 || board[58] == board[59] && board[58] != 0 || board[58] == board[60] && board[58] != 0 || board[58] == board[61] && board[58] != 0 || board[58] == board[62] && board[58] != 0 || board[58] == board[63] && board[58] != 0){
            gameOver();
        }else if(board[59] == board[55] && board[59] != 0 || board[59] == board[56] && board[59] != 0 || board[59] == board[57] && board[59] != 0 || board[59] == board[58] && board[59] != 0 || board[59] == board[60] && board[59] != 0 || board[59] == board[61] && board[59] != 0 || board[59] == board[62] && board[59] != 0 || board[59] == board[63] && board[59] != 0){
            gameOver();
        }else if(board[60] == board[55] && board[60] != 0 || board[60] == board[56] && board[60] != 0 || board[60] == board[57] && board[60] != 0 || board[60] == board[58] && board[60] != 0 || board[60] == board[59] && board[60] != 0 || board[60] == board[61] && board[60] != 0 || board[60] == board[62] && board[60] != 0 || board[60] == board[63] && board[60] != 0){
            gameOver();
        }else if(board[61] == board[55] && board[61] != 0 || board[61] == board[56] && board[61] != 0 || board[61] == board[57] && board[61] != 0 || board[61] == board[58] && board[61] != 0 || board[61] == board[59] && board[61] != 0 || board[61] == board[60] && board[61] != 0 || board[61] == board[62] && board[61] != 0 || board[61] == board[63] && board[61] != 0){
            gameOver();
        }else if(board[62] == board[55] && board[62] != 0 || board[62] == board[56] && board[62] != 0 || board[62] == board[57] && board[62] != 0 || board[62] == board[58] && board[62] != 0 || board[62] == board[59] && board[62] != 0 || board[62] == board[60] && board[62] != 0 || board[62] == board[61] && board[62] != 0 || board[62] == board[63] && board[62] != 0){
            gameOver();
        }else if(board[63] == board[55] && board[63] != 0 || board[63] == board[56] && board[63] != 0 || board[63] == board[57] && board[63] != 0 || board[63] == board[58] && board[63] != 0 || board[63] == board[59] && board[63] != 0 || board[62] == board[60] && board[63] != 0 || board[63] == board[61] && board[63] != 0 || board[63] == board[62] && board[63] != 0){
            gameOver();
        }

        else if(board[64] == board[65] && board[64] != 0 || board[64] == board[66] && board[64] != 0 || board[64] == board[67] && board[64] != 0 || board[64] == board[68] && board[64] != 0 || board[64] == board[69] && board[64] != 0 || board[64] == board[70] && board[64] != 0 || board[64] == board[71] && board[64] != 0 || board[64] == board[72] && board[64] != 0){
            gameOver();
        }else if(board[65] == board[64] && board[65] != 0 || board[65] == board[66] && board[65] != 0 || board[65] == board[67] && board[65] != 0 || board[65] == board[68] && board[65] != 0 || board[65] == board[69] && board[65] != 0 || board[65] == board[70] && board[65] != 0 || board[65] == board[71] && board[65] != 0 || board[65] == board[72] && board[65] != 0){
            gameOver();
        }else if(board[66] == board[64] && board[66] != 0 || board[66] == board[65] && board[66] != 0 || board[66] == board[67] && board[66] != 0 || board[66] == board[68] && board[66] != 0 || board[66] == board[69] && board[66] != 0 || board[66] == board[70] && board[66] != 0 || board[66] == board[71] && board[66] != 0 || board[66] == board[72] && board[66] != 0){
            gameOver();
        }else if(board[67] == board[64] && board[67] != 0 || board[67] == board[65] && board[67] != 0 || board[67] == board[66] && board[67] != 0 || board[67] == board[68] && board[67] != 0 || board[67] == board[69] && board[67] != 0 || board[67] == board[70] && board[67] != 0 || board[67] == board[71] && board[67] != 0 || board[67] == board[72] && board[67] != 0){
            gameOver();
        }else if(board[68] == board[64] && board[68] != 0 || board[68] == board[65] && board[68] != 0 || board[68] == board[66] && board[68] != 0 || board[68] == board[67] && board[68] != 0 || board[68] == board[69] && board[68] != 0 || board[68] == board[70] && board[68] != 0 || board[68] == board[71] && board[68] != 0 || board[68] == board[72] && board[68] != 0){
            gameOver();
        }else if(board[69] == board[64] && board[69] != 0 || board[69] == board[65] && board[69] != 0 || board[69] == board[66] && board[69] != 0 || board[69] == board[67] && board[69] != 0 || board[69] == board[68] && board[69] != 0 || board[69] == board[70] && board[69] != 0 || board[69] == board[71] && board[69] != 0 || board[69] == board[72] && board[69] != 0){
            gameOver();
        }else if(board[70] == board[64] && board[70] != 0 || board[70] == board[65] && board[70] != 0 || board[70] == board[66] && board[70] != 0 || board[70] == board[67] && board[70] != 0 || board[70] == board[68] && board[70] != 0 || board[70] == board[69] && board[70] != 0 || board[70] == board[71] && board[70] != 0 || board[70] == board[72] && board[70] != 0){
            gameOver();
        }else if(board[71] == board[64] && board[71] != 0 || board[71] == board[65] && board[71] != 0 || board[71] == board[66] && board[71] != 0 || board[71] == board[67] && board[71] != 0 || board[71] == board[68] && board[71] != 0 || board[71] == board[69] && board[71] != 0 || board[71] == board[70] && board[71] != 0 || board[71] == board[72] && board[71] != 0){
            gameOver();
        }else if(board[72] == board[64] && board[72] != 0 || board[72] == board[65] && board[72] != 0 || board[72] == board[66] && board[72] != 0 || board[72] == board[67] && board[72] != 0 || board[72] == board[68] && board[72] != 0 || board[72] == board[69] && board[72] != 0 || board[72] == board[70] && board[72] != 0 || board[72] == board[71] && board[72] != 0){
            gameOver();
        }

        else if(board[73] == board[74] && board[73] != 0 || board[73] == board[75] && board[73] != 0 || board[73] == board[76] && board[73] != 0 || board[73] == board[77] && board[73] != 0 || board[73] == board[78] && board[73] != 0 || board[73] == board[79] && board[73] != 0 || board[73] == board[80] && board[73] != 0 || board[73] == board[81] && board[73] != 0){
            gameOver();
        }else if(board[74] == board[73] && board[74] != 0 || board[74] == board[75] && board[74] != 0 || board[74] == board[76] && board[74] != 0 || board[74] == board[77] && board[74] != 0 || board[74] == board[78] && board[74] != 0 || board[74] == board[79] && board[74] != 0 || board[74] == board[80] && board[74] != 0 || board[74] == board[81] && board[74] != 0){
            gameOver();
        }else if(board[75] == board[73] && board[75] != 0 || board[75] == board[74] && board[75] != 0 || board[75] == board[76] && board[75] != 0 || board[75] == board[77] && board[75] != 0 || board[75] == board[78] && board[75] != 0 || board[75] == board[79] && board[75] != 0 || board[75] == board[80] && board[75] != 0 || board[75] == board[81] && board[75] != 0){
            gameOver();
        }else if(board[76] == board[73] && board[76] != 0 || board[76] == board[74] && board[76] != 0 || board[76] == board[75] && board[76] != 0 || board[76] == board[77] && board[76] != 0 || board[76] == board[78] && board[76] != 0 || board[76] == board[79] && board[76] != 0 || board[76] == board[80] && board[76] != 0 || board[76] == board[81] && board[76] != 0){
            gameOver();
        }else if(board[77] == board[73] && board[77] != 0 || board[77] == board[74] && board[77] != 0 || board[77] == board[75] && board[77] != 0 || board[77] == board[76] && board[77] != 0 || board[77] == board[78] && board[77] != 0 || board[77] == board[79] && board[77] != 0 || board[77] == board[80] && board[77] != 0 || board[77] == board[81] && board[77] != 0){
            gameOver();
        }else if(board[78] == board[73] && board[78] != 0 || board[78] == board[74] && board[78] != 0 || board[78] == board[75] && board[78] != 0 || board[78] == board[76] && board[78] != 0 || board[78] == board[77] && board[78] != 0 || board[78] == board[79] && board[78] != 0 || board[78] == board[80] && board[78] != 0 || board[78] == board[81] && board[78] != 0){
            gameOver();
        }else if(board[79] == board[73] && board[79] != 0 || board[79] == board[74] && board[79] != 0 || board[79] == board[75] && board[79] != 0 || board[79] == board[76] && board[79] != 0 || board[79] == board[77] && board[79] != 0 || board[79] == board[78] && board[79] != 0 || board[79] == board[80] && board[79] != 0 || board[79] == board[81] && board[79] != 0){
            gameOver();
        }else if(board[80] == board[73] && board[80] != 0 || board[80] == board[74] && board[80] != 0 || board[80] == board[75] && board[80] != 0 || board[80] == board[76] && board[80] != 0 || board[80] == board[77] && board[80] != 0 || board[80] == board[78] && board[80] != 0 || board[80] == board[79] && board[80] != 0 || board[80] == board[81] && board[80] != 0){
            gameOver();
        }else if(board[81] == board[73] && board[81] != 0 || board[81] == board[74] && board[81] != 0 || board[81] == board[75] && board[81] != 0 || board[81] == board[76] && board[81] != 0 || board[81] == board[77] && board[81] != 0 || board[81] == board[78] && board[81] != 0 || board[81] == board[79] && board[81] != 0 || board[81] == board[80] && board[81] != 0){
            gameOver();
        }
    }

    public static void main(String[] args){
        // ketentuan:
        // 1. jika terdapat angka yang sama pada setiap baris maka game over
        // 2. game akan selesai ketika setiap baris, kolom, dan kotak 3x3 tidak ada angka yang sama
        membuatSudoku obj = new membuatSudoku();
        while(obj.isCompleted && obj.loser) {
            obj.display();
            obj.analisis();
            obj.pilihLetak();
            obj.analisis2();
            obj.analisis3();
            obj.analisis4();
        }
    }
}
